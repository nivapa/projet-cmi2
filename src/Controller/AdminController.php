<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Form\AdminRegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin-inscription", name="admin_registration")
     */
    public function adminRegistration(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface
    $encoder ) {
        $admin = new Admin();

        $form = $this->createForm(AdminRegistrationType::class, $admin);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($admin, $admin->getPassword());
            $admin->setPassword($hash);

            $manager->persist($admin);
            $manager->flush();

            return $this->redirectToRoute('admin_login');

        }

        return $this->render('admin/admin-registration.html.twig', [
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/admin", name="admin_login")
     */
    public function login() {
        return $this->render('back/index.html.twig');
    }
}
