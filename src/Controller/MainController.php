<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ContactType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        /* 
    pour vérifier que l'utilisateur à bien modifier son MDP à la 1ere connection
    on ajoute une propriété is_first_login définie à true.
    A chaque fois que l'utilisateur va vouloir se connecter, on vérifie cette propriété, 
    si true on affiche le formulaire de changement de MDP, sinon on va sur la page d'accueil
    
    */

        if($this->getUSer() && $this->getUSer()->getIsFirstLogin()){
            $this->redirectToRoute('modif_mdp');
        }

        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/club", name="club")
     */
    public function club()
    {
        return $this->render('main/club.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/actu", name="actu")
     */
    public function actu(ArticleRepository $repo)
    {

        $article=$repo->findAll();

        dump($article);

        return $this->render('main/actu.html.twig', [
            'controller_name' => 'MainController',
            'articles' => $article
        ]);
    }

    /**
     * @Route("/actu_show/{id}", name="actu_show")
     */
    public function actu_show(Article $article)
    {
        return $this->render('main/actu_show.html.twig', [
            'article' =>$article
        ]);
    }

    /**
     * @Route("/equipes", name="equipes")
     */
    public function equipes()
    {
        return $this->render('main/equipes.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/planning", name="planning")
     */
    public function planning()
    {
        return $this->render('main/planning.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/sponsors", name="sponsors")
     */
    public function sponsors()
    {
        return $this->render('main/sponsors.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->flush();
        }

        return $this->render('main/contact.html.twig', [
            'controller_name' => 'MainController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/mentions", name="mentions")
     */
    public function mentions()
    {
        return $this->render('main/mentions.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/plan", name="plan")
     */
    public function plan()
    {
        return $this->render('main/plan.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }
}
