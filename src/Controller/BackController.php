<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BackController extends AbstractController
{

    /**
     * @Route("/accueil-bo", name="accueil-bo")
     */
    public function accueil()
    {
        return $this->render('back/accueil.html.twig', [
            'controller_name' => 'BackController',
        ]);
    }

    /**
     * @Route("/list-user", name="list-user")
     */
    public function list()
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $users = $userRepository->findAll();
        return $this->render('back/liste-user.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/accueil-bo/new", name="article-create")
     * @Route("/actu_show{id}/edit", name="article-edit")
     */
    public function form(Article $article=null, Request $request, EntityManagerInterface $manager)
    {
        if(!$article){
            $article= new Article();
        }

            $form = $this->createForm(ArticleType::class,$article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() ) {
            if(!$article->getId()){
                
            }

            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('show_actu',['id'=>$article->getId()]);
        }
        

        return $this->render('back/create.html.twig', [
            'controller_name' => 'BackController',
            'formArticle'=>$form->createView(),
            'editmode'=>$article->getId()!==null
        ]);
    }

    /**
     * @Route("/actu_show/{id}", name="show_actu")
     */
    public function actu_show(Article $article)
    {
        return $this->render('back/actu_show.html.twig', [
            'article' =>$article
        ]);
    }
}