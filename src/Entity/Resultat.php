<?php

namespace App\Entity;

use App\Repository\ResultatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ResultatRepository::class)
 */
class Resultat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $score_eux;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $score_nous;

    /**
     * @ORM\ManyToOne(targetEntity=Equipe::class, inversedBy="resultats")
     */
    private $equipes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_eux;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_nous;

    /**
     * @ORM\ManyToOne(targetEntity=Evenement::class, inversedBy="resultats")
     */
    private $evenement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScoreEux(): ?string
    {
        return $this->score_eux;
    }

    public function setScoreEux(?string $score_eux): self
    {
        $this->score_eux = $score_eux;

        return $this;
    }

    public function getScoreNous(): ?string
    {
        return $this->score_nous;
    }

    public function setScoreNous(?string $score_nous): self
    {
        $this->score_nous = $score_nous;

        return $this;
    }

    public function getEquipes(): ?Equipe
    {
        return $this->equipes;
    }

    public function setEquipes(?Equipe $equipes): self
    {
        $this->equipes = $equipes;

        return $this;
    }

    public function getNomEux(): ?string
    {
        return $this->nom_eux;
    }

    public function setNomEux(string $nom_eux): self
    {
        $this->nom_eux = $nom_eux;

        return $this;
    }

    public function getNomNous(): ?string
    {
        return $this->nom_nous;
    }

    public function setNomNous(string $nom_nous): self
    {
        $this->nom_nous = $nom_nous;

        return $this;
    }

    public function getEvenement(): ?Evenement
    {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): self
    {
        $this->evenement = $evenement;

        return $this;
    }
}
