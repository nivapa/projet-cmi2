<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EquipeController extends AbstractController
{
    /**
     * @Route("/hommes", name="hommes")
     */
    public function hommes()
    {
        return $this->render('equipe/hommes.html.twig', [
            'controller_name' => 'EquipeController',
        ]);
    }

    /**
     * @Route("/femmes", name="femmes")
     */
    public function femmes()
    {
        return $this->render('equipe/femmes.html.twig', [
            'controller_name' => 'EquipeController',
        ]);
    }

    /**
     * @Route("/garcons", name="garcons")
     */
    public function garcons()
    {
        return $this->render('equipe/garcons.html.twig', [
            'controller_name' => 'EquipeController',
        ]);
    }

    /**
     * @Route("/filles", name="filles")
     */
    public function filles()
    {
        return $this->render('equipe/filles.html.twig', [
            'controller_name' => 'EquipeController',
        ]);
    }
}
