<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Article;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Création de 10 articles
        for ($i=1; $i < 10; $i++) {
            // J'instancie un article à partir de la classe Article
            $article=new Article();
            // Avec les setter je donne des valeurs aux propriétes
            $article->setTitre("Titre de l'article n° ".$i)
            ->setContenu("Contenu de l'article n°" .$i)
            ->setImage("Image de l'article n°" .$i);
            

            // Je demande à mon manager de stocker les données
            $manager->persist($article);
        }      
        // Je demande à mon manager d'écrire dans la table article tous les articles stockés
        $manager->flush();
    }
}
